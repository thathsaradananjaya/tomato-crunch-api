package com.tomato.tomatocrunchapi.service;

import com.tomato.tomatocrunchapi.dto.FilterDto;
import com.tomato.tomatocrunchapi.dto.GameDto;
import com.tomato.tomatocrunchapi.dto.wrapper.WrapperGameDetail;

public interface GameService {

    GameDto saveGameData(GameDto gameDto);

    GameDto getGameData(String username);

    WrapperGameDetail getLeaderboardData(FilterDto filterDto);

    GameDto getProfileData(String username);

    GameDto restartProgress(String username);

}
