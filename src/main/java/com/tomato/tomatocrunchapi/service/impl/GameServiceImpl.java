package com.tomato.tomatocrunchapi.service.impl;

import com.tomato.tomatocrunchapi.dto.FilterDto;
import com.tomato.tomatocrunchapi.dto.GameDto;
import com.tomato.tomatocrunchapi.dto.wrapper.WrapperGameDetail;
import com.tomato.tomatocrunchapi.entity.Game;
import com.tomato.tomatocrunchapi.entity.User;
import com.tomato.tomatocrunchapi.exception.RecordNotFoundException;
import com.tomato.tomatocrunchapi.repository.GameRepository;
import com.tomato.tomatocrunchapi.repository.UserRepository;
import com.tomato.tomatocrunchapi.service.GameService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@AllArgsConstructor
@Slf4j
@Transactional
@Service
public class GameServiceImpl implements GameService {
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final GameRepository gameRepository;

    @Override
    public GameDto saveGameData(GameDto gameDto) {
        Optional<Game> gameByUsername = gameRepository.findByUser_Username(gameDto.getUsername());
        Game savedGame;
        if (gameByUsername.isPresent()) {
            Game game = gameByUsername.get();
            game.setLevel(gameDto.getLevel());
            game.setBestTime(getBestTime(game.getBestTime(), gameDto.getTime()));
            game.setWorstTime(getWorstTime(game.getWorstTime(), gameDto.getTime()));
            game.setHighestScore(getHighestScore(game.getHighestScore(), gameDto.getScore()));
            game.setLowestScore(getLowestScore(game.getLowestScore(), gameDto.getScore()));
            game.setTotalScore(getTotalScore(game.getTotalScore(), gameDto.getScore()));
            game.setScore(gameDto.getScore());
            savedGame = gameRepository.save(game);
        } else {
            Optional<User> byUsername = userRepository.findByUsername(gameDto.getUsername());
            if (byUsername.isPresent()) {
                Game game = new Game();
                game.setUser(byUsername.get());
                game.setLevel(gameDto.getLevel());
                game.setBestTime(gameDto.getTime());
                game.setWorstTime(gameDto.getTime());
                game.setHighestScore(gameDto.getScore());
                game.setLowestScore(gameDto.getScore());
                game.setTotalScore(gameDto.getScore());
                game.setScore(gameDto.getScore());
                savedGame = gameRepository.save(game);
            } else {
                throw new RecordNotFoundException("User not found");
            }
        }
        return savedGame.toDto(modelMapper);
    }

    @Override
    public GameDto getGameData(String username) {
        Optional<Game> gameByUsername = gameRepository.findByUser_Username(username);
        if (gameByUsername.isPresent()) {
            return gameByUsername.get().toDto(modelMapper);
        } else {
            return GameDto.builder().id(null).username(username).level(1).build();
        }
    }

    @Override
    public WrapperGameDetail getLeaderboardData(FilterDto filterDto) {
        PageRequest pageRequest = PageRequest.of((filterDto.getPage() -1), filterDto.getSize(), Sort.by(Sort.Direction.valueOf(filterDto.getSortOrder()), filterDto.getSortField()));
        Page<Game> all = gameRepository.findAll(pageRequest);
        AtomicLong rowId = new AtomicLong((long) pageRequest.getPageNumber() * pageRequest.getPageSize());
        List<GameDto> gameDtoList = all.stream().map(game -> {
            GameDto gameDto = game.toDto(modelMapper);
            gameDto.setKey(rowId.incrementAndGet());
            return gameDto;
        }).toList();
        return WrapperGameDetail.builder().total(all.getTotalElements()).gameDtoList(gameDtoList).build();
    }

    @Override
    public GameDto getProfileData(String username) {
        Optional<Game> gameByUsername = gameRepository.findByUser_Username(username);
        if (gameByUsername.isPresent()) {
            return gameByUsername.get().toDto(modelMapper);
        } else {
            return GameDto.builder()
                    .username(username)
                    .level(1)
                    .bestTime(0)
                    .worstTime(0)
                    .highestScore(0)
                    .lowestScore(0)
                    .totalScore(0).build();
        }
    }

    @Override
    public GameDto restartProgress(String username) {
        Optional<Game> gameByUsername = gameRepository.findByUser_Username(username);
        Game savedGame;
        if (gameByUsername.isPresent()) {
            Game game = gameByUsername.get();
            game.setLevel(1);
            game.setTotalScore(0L);
            game.setScore(null);
            savedGame = gameRepository.save(game);
        } else {
            savedGame = new Game();
            savedGame.setLevel(1);
            savedGame.setTotalScore(0L);
        }
        return savedGame.toDto(modelMapper);
    }

    private long getTotalScore(Long currentScore, Long score) {
        if (null != currentScore) {
            return currentScore + score;
        } else {
            return  score;
        }
    }

    private long getHighestScore(Long currentHighest, Long score) {
        if (null != currentHighest) {
            return Math.max(currentHighest, score);
        } else {
            return  score;
        }
    }

    private long getLowestScore(Long currentLowest, Long score) {
        if (null != currentLowest) {
            return Math.min(currentLowest, score);
        } else {
            return  score;
        }
    }

    private int getBestTime(Integer currentBest, Integer time) {
        if (null != currentBest) {
            return Math.min(currentBest, time);
        } else {
            return time;
        }
    }

    private int getWorstTime(Integer currentWorst, Integer time) {
        if (null != currentWorst) {
            return Math.max(currentWorst, time);
        } else {
            return time;
        }
    }
}
