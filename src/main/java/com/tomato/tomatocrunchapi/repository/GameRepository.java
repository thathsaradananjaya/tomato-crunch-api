package com.tomato.tomatocrunchapi.repository;

import com.tomato.tomatocrunchapi.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GameRepository extends JpaRepository<Game, Integer> {

    Optional<Game> findByUser_Username(String username);

}
