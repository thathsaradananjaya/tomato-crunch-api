package com.tomato.tomatocrunchapi.dto;

import lombok.Data;

@Data
public class FilterDto {
    private int page;
    private int size;
    private String sortField;
    private String sortOrder;
}
