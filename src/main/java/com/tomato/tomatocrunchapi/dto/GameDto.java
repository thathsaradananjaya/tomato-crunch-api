package com.tomato.tomatocrunchapi.dto;

import com.tomato.tomatocrunchapi.entity.Game;
import lombok.*;
import org.modelmapper.ModelMapper;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GameDto {
    private Integer id;
    private String username;
    private int level;
    private int bestTime;
    private int worstTime;
    private long highestScore;
    private long lowestScore;
    private long totalScore;

    private int time;
    private long score;
    private Long key;

    public Game toEntity(ModelMapper modelMapper) {
        return modelMapper.map(this, Game.class);
    }
}
