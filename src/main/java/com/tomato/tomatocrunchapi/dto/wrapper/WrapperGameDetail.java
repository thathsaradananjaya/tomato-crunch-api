package com.tomato.tomatocrunchapi.dto.wrapper;

import com.tomato.tomatocrunchapi.dto.GameDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WrapperGameDetail {
    private Long total = 0L;
    private List<GameDto> gameDtoList;
}
