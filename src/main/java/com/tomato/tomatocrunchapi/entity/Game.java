package com.tomato.tomatocrunchapi.entity;

import com.tomato.tomatocrunchapi.dto.GameDto;
import jakarta.persistence.*;
import lombok.*;
import org.modelmapper.ModelMapper;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    private User user;
    private Integer level;
    private Integer bestTime;
    private Integer worstTime;
    private Long highestScore;
    private Long lowestScore;
    private Long totalScore;
    private Long score;

    public GameDto toDto(ModelMapper modelMapper) {
        GameDto gameDto = modelMapper.map(this, GameDto.class);
        if (null != this.user)
            gameDto.setUsername(this.user.getUsername());
        return gameDto;
    }
}
