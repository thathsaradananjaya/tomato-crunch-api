package com.tomato.tomatocrunchapi.exception;

public class JdbcException extends RuntimeException {
    public JdbcException() {}

    public JdbcException(String message) {
        super(message);
    }

    public JdbcException(String message, Throwable cause) {
        super(message, cause);
    }

}
