package com.tomato.tomatocrunchapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RequiredValueException extends RuntimeException {
    public RequiredValueException() {}

    public RequiredValueException(String message) {
        super(message);
    }

    public RequiredValueException(String message, Throwable cause) {
        super(message, cause);
    }

}
