package com.tomato.tomatocrunchapi.exception;

public class UnAuthorizeException extends RuntimeException {
    public UnAuthorizeException() {}

    public UnAuthorizeException(String message) {
        super(message);
    }

    public UnAuthorizeException(String message, Throwable cause) {
        super(message, cause);
    }

}
