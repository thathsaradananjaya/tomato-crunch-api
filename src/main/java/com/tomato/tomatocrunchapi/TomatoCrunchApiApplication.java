package com.tomato.tomatocrunchapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TomatoCrunchApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TomatoCrunchApiApplication.class, args);
    }

}
