package com.tomato.tomatocrunchapi.controller;

import com.tomato.tomatocrunchapi.dto.FilterDto;
import com.tomato.tomatocrunchapi.dto.wrapper.WrapperGameDetail;
import com.tomato.tomatocrunchapi.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/leaderboard-service")
@RequiredArgsConstructor
public class LeaderboardController {

    private final GameService gameService;

    @PostMapping("/get-table-data")
    public ResponseEntity<WrapperGameDetail> getLeaderboardData(@RequestBody FilterDto filterDto) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.getLeaderboardData(filterDto));
    }

}
