package com.tomato.tomatocrunchapi.controller;

import com.tomato.tomatocrunchapi.dto.GameDto;
import com.tomato.tomatocrunchapi.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/game-service")
@RequiredArgsConstructor
public class GameController {

    private final GameService gameService;


    @GetMapping("/get-game-data/{username}")
    public ResponseEntity<GameDto> getGameData(@PathVariable String username) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.getGameData(username));
    }

    @PostMapping("/save-game-data")
    public ResponseEntity<GameDto> saveGame(@RequestBody GameDto gameDto) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.saveGameData(gameDto));
    }

    @GetMapping("/restart/{username}")
    public ResponseEntity<GameDto> restartProgress(@PathVariable String username) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.restartProgress(username));
    }
}
