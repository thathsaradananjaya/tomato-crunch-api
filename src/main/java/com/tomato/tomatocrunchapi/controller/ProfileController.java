package com.tomato.tomatocrunchapi.controller;

import com.tomato.tomatocrunchapi.dto.GameDto;
import com.tomato.tomatocrunchapi.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/profile-service")
@RequiredArgsConstructor
public class ProfileController {

    private final GameService gameService;

    @GetMapping("/get-profile-data/{username}")
    public ResponseEntity<GameDto> getProfileData(@PathVariable  String username) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.getProfileData(username));
    }
}
